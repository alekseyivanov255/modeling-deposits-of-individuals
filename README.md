Если markdown файл некорректно отображает некоторые блоки, то, пожалуйста, скачайте и смотрите файл "finmodeling.html".


Загрузим данные из файла.


```python
import pandas as pd
data = pd.read_excel("BST_DataSet.xlsx")
```

Посмотрим на содержимое таблицы.


```python
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>Прирост вкладов физических лиц в рублях (млн руб)</th>
      <th>Доходность ОФЗ по сроку до 1 года</th>
      <th>Ключевая ставка</th>
      <th>Ставка по вкладам в долларах до 1 года</th>
      <th>Ставка по вкладам в рублях до 1 года</th>
      <th>Нефть марки Юралс, долл./барр</th>
      <th>Индекс потребительских цен, ед.</th>
      <th>М0, руб</th>
      <th>М2, руб</th>
      <th>Курс доллара к рублю, руб</th>
      <th>Номинальная средняя з/п, руб</th>
      <th>Unnamed: 12</th>
      <th>Unnamed: 13</th>
      <th>Unnamed: 14</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2011-01-01</td>
      <td>-79049.0</td>
      <td>5.244667</td>
      <td>7.75</td>
      <td>2.90</td>
      <td>4.50</td>
      <td>96.29</td>
      <td>106.171510</td>
      <td>5.532900e+06</td>
      <td>19307.700000</td>
      <td>30.08</td>
      <td>20669</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2011-02-01</td>
      <td>180475.0</td>
      <td>5.144211</td>
      <td>8.00</td>
      <td>3.10</td>
      <td>4.40</td>
      <td>103.96</td>
      <td>106.999640</td>
      <td>5.613700e+06</td>
      <td>19536.700000</td>
      <td>29.29</td>
      <td>20680</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2011-03-01</td>
      <td>106845.0</td>
      <td>5.021364</td>
      <td>8.00</td>
      <td>3.10</td>
      <td>4.20</td>
      <td>114.44</td>
      <td>107.663040</td>
      <td>5.647000e+06</td>
      <td>19788.700000</td>
      <td>28.43</td>
      <td>22673</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2011-04-01</td>
      <td>224890.0</td>
      <td>4.558571</td>
      <td>8.00</td>
      <td>2.60</td>
      <td>4.10</td>
      <td>123.15</td>
      <td>108.125990</td>
      <td>5.863400e+06</td>
      <td>20020.800000</td>
      <td>28.10</td>
      <td>22519</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2011-05-01</td>
      <td>45856.0</td>
      <td>4.795000</td>
      <td>8.25</td>
      <td>2.60</td>
      <td>4.10</td>
      <td>114.46</td>
      <td>108.645000</td>
      <td>5.893400e+06</td>
      <td>20160.900000</td>
      <td>27.87</td>
      <td>22779</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>79</th>
      <td>2017-08-01</td>
      <td>NaN</td>
      <td>7.882174</td>
      <td>9.00</td>
      <td>0.73</td>
      <td>6.07</td>
      <td>52.00</td>
      <td>168.418880</td>
      <td>9.194600e+06</td>
      <td>39419.300000</td>
      <td>59.65</td>
      <td>37099</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>80</th>
      <td>2017-09-01</td>
      <td>NaN</td>
      <td>7.648095</td>
      <td>8.50</td>
      <td>0.66</td>
      <td>5.31</td>
      <td>57.00</td>
      <td>168.166250</td>
      <td>9.225200e+06</td>
      <td>39571.000000</td>
      <td>57.73</td>
      <td>38047</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>81</th>
      <td>2017-10-01</td>
      <td>NaN</td>
      <td>7.503636</td>
      <td>8.25</td>
      <td>0.61</td>
      <td>5.67</td>
      <td>57.50</td>
      <td>168.502590</td>
      <td>9.246600e+06</td>
      <td>39565.982237</td>
      <td>57.71</td>
      <td>38333</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>82</th>
      <td>2017-11-01</td>
      <td>NaN</td>
      <td>7.381429</td>
      <td>8.25</td>
      <td>0.64</td>
      <td>5.18</td>
      <td>63.10</td>
      <td>168.852459</td>
      <td>9.281212e+06</td>
      <td>40101.582544</td>
      <td>58.92</td>
      <td>38848</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>83</th>
      <td>2017-12-01</td>
      <td>NaN</td>
      <td>6.978095</td>
      <td>7.75</td>
      <td>0.73</td>
      <td>5.27</td>
      <td>64.90</td>
      <td>169.560950</td>
      <td>9.974472e+06</td>
      <td>42372.101217</td>
      <td>58.59</td>
      <td>50500</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>84 rows × 15 columns</p>
</div>



Подправим название первой колонки с датами. Оставим только колонки с данными, пустые оставим.


```python
data.rename(columns = {'Unnamed: 0': 'Date'}, inplace = True)  
data = data.loc[:, data.columns[0:12]] 
```

Разметим часть, где train и test. Разметим часть, где нужно уже предсказать target.


```python
train_test_set = data.loc[0:71, ]
predict_set = data.loc[72:len(data),]
```

Определим, какая target переменнная, а какие объясняющие. 


```python
y_name = 'Прирост вкладов физических лиц в рублях (млн руб)'
x = pd.DataFrame(train_test_set.drop([y_name, 'Date'],axis=1)) 
y = pd.DataFrame(train_test_set.loc[:, y_name])
x_new_pred = pd.DataFrame(predict_set.drop([y_name, 'Date'],axis=1)) # Объяснющие переменные для тех значений, которые нужно предсказать.
```

Выделим train и test.


```python
# from sklearn.model_selection import train_test_split
# x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.33,random_state=84) # можно разделить на 2 непересекающиеся части train и test

x_train = x # Но лучше используем все
x_test = x # используем все
y_train = y # используем все
y_test = y # используем все
```

Используем библиотеки scikit-learn для модели линейной регрессии и matplotlib для рисования графиков.


```python
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
```

Попытаемся использовать линейную регрессию. 
\begin{align}
y =\sum_{i=1}^{N} \omega_{i}^{} x_{i}^{} + \omega _{0}^{}
\end{align}


```python
regr = linear_model.LinearRegression()
regr.fit(x_train, y_train)
y_pred = regr.predict(x_test)
```

Посмотрим на коеффициенты в результате работы модели.


```python
print("Intercept: \n", regr.intercept_)
```

    Intercept: 
     [-1476506.38838737]
    

Коэффициент пересечения.


```python
print("Coefficients: \n", regr.coef_.tolist()[0])
```

    Coefficients: 
     [-58229.735880222, -25059.406893679778, 178220.76075323805, -50183.366739547026, 4011.149069920082, 9490.396917924963, -0.17289245336360182, -41.37028419322283, 12159.379560279576, 83.49767064054748]
    

Посмотрим на коэффициенты в таблице. Заметно, что самый важная фича здесь "Ставка по вкладам в долларах до 1 года". Target иммет положительную связь с ней.


```python
fin_coeff_table = pd.DataFrame({'Объяснящие признаки': x_test.columns, 'Коэффициенты': np.around(regr.coef_.tolist()[0], decimals=1)})
fin_coeff_table.style.bar(subset=['Коэффициенты'], color='#d65f5f', align='mid')
```




<style  type="text/css" >
#T_87130_row0_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg,#d65f5f 24.6%, transparent 24.6%);
        }#T_87130_row1_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 14.0%, #d65f5f 14.0%, #d65f5f 24.6%, transparent 24.6%);
        }#T_87130_row2_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 100.0%, transparent 100.0%);
        }#T_87130_row3_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 3.4%, #d65f5f 3.4%, #d65f5f 24.6%, transparent 24.6%);
        }#T_87130_row4_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 26.3%, transparent 26.3%);
        }#T_87130_row5_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 28.6%, transparent 28.6%);
        }#T_87130_row6_col1,#T_87130_row7_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 24.6%, transparent 24.6%);
        }#T_87130_row8_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 29.8%, transparent 29.8%);
        }#T_87130_row9_col1{
            width:  10em;
             height:  80%;
            background:  linear-gradient(90deg, transparent 24.6%, #d65f5f 24.6%, #d65f5f 24.7%, transparent 24.7%);
        }</style><table id="T_87130_" ><thead>    <tr>        <th class="blank level0" ></th>        <th class="col_heading level0 col0" >Объяснящие признаки</th>        <th class="col_heading level0 col1" >Коэффициенты</th>    </tr></thead><tbody>
                <tr>
                        <th id="T_87130_level0_row0" class="row_heading level0 row0" >0</th>
                        <td id="T_87130_row0_col0" class="data row0 col0" >Доходность ОФЗ по сроку до 1 года</td>
                        <td id="T_87130_row0_col1" class="data row0 col1" >-58229.700000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row1" class="row_heading level0 row1" >1</th>
                        <td id="T_87130_row1_col0" class="data row1 col0" >Ключевая ставка</td>
                        <td id="T_87130_row1_col1" class="data row1 col1" >-25059.400000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row2" class="row_heading level0 row2" >2</th>
                        <td id="T_87130_row2_col0" class="data row2 col0" >Ставка по вкладам в долларах до 1 года</td>
                        <td id="T_87130_row2_col1" class="data row2 col1" >178220.800000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row3" class="row_heading level0 row3" >3</th>
                        <td id="T_87130_row3_col0" class="data row3 col0" >Ставка по вкладам в рублях до 1 года</td>
                        <td id="T_87130_row3_col1" class="data row3 col1" >-50183.400000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row4" class="row_heading level0 row4" >4</th>
                        <td id="T_87130_row4_col0" class="data row4 col0" >Нефть марки Юралс, долл./барр</td>
                        <td id="T_87130_row4_col1" class="data row4 col1" >4011.100000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row5" class="row_heading level0 row5" >5</th>
                        <td id="T_87130_row5_col0" class="data row5 col0" >Индекс потребительских цен, ед.</td>
                        <td id="T_87130_row5_col1" class="data row5 col1" >9490.400000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row6" class="row_heading level0 row6" >6</th>
                        <td id="T_87130_row6_col0" class="data row6 col0" >М0, руб</td>
                        <td id="T_87130_row6_col1" class="data row6 col1" >-0.200000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row7" class="row_heading level0 row7" >7</th>
                        <td id="T_87130_row7_col0" class="data row7 col0" >М2, руб</td>
                        <td id="T_87130_row7_col1" class="data row7 col1" >-41.400000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row8" class="row_heading level0 row8" >8</th>
                        <td id="T_87130_row8_col0" class="data row8 col0" >Курс доллара к рублю, руб</td>
                        <td id="T_87130_row8_col1" class="data row8 col1" >12159.400000</td>
            </tr>
            <tr>
                        <th id="T_87130_level0_row9" class="row_heading level0 row9" >9</th>
                        <td id="T_87130_row9_col0" class="data row9 col0" >Номинальная средняя з/п, руб</td>
                        <td id="T_87130_row9_col1" class="data row9 col1" >83.500000</td>
            </tr>
    </tbody></table>



Можно посмотреть на среднеквадратичную ошибку, но пока она не даст понимания качества модели, т.к. нет сравнения.


```python
print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
```

    Mean squared error: 29062498707.77
    

Посмотрим на коэффициент R2.


```python
print("Coefficient of determination: %.2f" % r2_score(y_test, y_pred))
```

    Coefficient of determination: 0.56
    

Построим график временной зависимости.


```python
ig, ax = plt.subplots()

ax.plot(data.Date, data.loc[:, y_name], label = 'Оригинал', color="blue")
ax.plot(data.Date, regr.predict(data.drop([y_name, 'Date'],axis=1)), label = 'Линейная регрессия', color="red")

ax.legend(fontsize = 10,
          ncol = 1,    #  количество столбцов
          facecolor = 'oldlace',    #  цвет области
          edgecolor = 'r',    #  цвет крайней линии
          title = '',    #  заголовок
          title_fontsize = '6'    #  размер шрифта заголовка
         )

plt.title(y_name, fontsize=6)
plt.ylabel('млн руб', fontsize=7)
plt.xlabel('Год', fontsize=7)
plt.rcParams['figure.dpi'] = 200
plt.rcParams['figure.figsize'] = [7.0, 3.2]
plt.show()

plt.show()
```


    
![png](output_27_0.png)
    


Заметим, что модель хорошо определяет тренд в окнах шириной в несколько месяцев. Также данная модель линейной регрессии предсказала очередной макимум прироста вкладов физических лиц (целевой переменной) к концу 2017 года, что можно было легко можно предугадать, просмотрев таблицу и график, где видна периодичность с максимумами в декабре и минимумами в январе. Найдена положительная зависимость предсказываемой переменной от "ставки по вкладам в долларах до 1 года" и отрицательная от "доходности ОФЗ по сроку от 1 года".
